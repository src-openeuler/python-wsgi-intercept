%global _empty_manifest_terminate_build 0
Name:           python-wsgi-intercept
Version:        1.13.1
Release:        1
Summary:        wsgi_intercept installs a WSGI application in place of a real URI for testing.
License:        MIT
URL:            http://pypi.python.org/pypi/wsgi_intercept
Source0:        %{pypi_source wsgi_intercept}
BuildArch:      noarch
%description
Testing a WSGI application sometimes involves starting a server at a
local host and port, then pointing your test code to that address.
Instead, this library lets you intercept calls to any specific host/port
combination and redirect them into a `WSGI application`_ importable by
your test program. Thus, you can avoid spawning multiple processes or
threads to test your Web app.

%package -n python3-wsgi-intercept
Summary:        wsgi_intercept installs a WSGI application in place of a real URI for testing.
Provides:       python-wsgi-intercept = %{version}-%{release}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
Requires:       python3-six
Requires:       python3-sphinx
# Tests running requires
Requires:       python3-pytest
Requires:       python3-httplib2
Requires:       python3-requests
Requires:       python3-urllib3
%description -n python3-wsgi-intercept
Testing a WSGI application sometimes involves starting a server at a
local host and port, then pointing your test code to that address.
Instead, this library lets you intercept calls to any specific host/port
combination and redirect them into a `WSGI application`_ importable by
your test program. Thus, you can avoid spawning multiple processes or
threads to test your Web app.

%package help
Summary:        wsgi_intercept installs a WSGI application in place of a real URI for testing.
Provides:       python3-wsgi-intercept-doc
%description help
Testing a WSGI application sometimes involves starting a server at a
local host and port, then pointing your test code to that address.
Instead, this library lets you intercept calls to any specific host/port
combination and redirect them into a `WSGI application`_ importable by
your test program. Thus, you can avoid spawning multiple processes or
threads to test your Web app.

%prep
%autosetup -n wsgi_intercept-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-wsgi-intercept -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 23 2024 Ge Wang <wang__ge@126.com> - 1.13.1-1
- Update package to version 1.13.1

* Mon Feb 26 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.13.0-1
- Update package to version 1.13.0

* Wed Jul 05 2023 chenzixuan <chenzixuan@kylinos.cn> - 1.12.1-1
- Upgrade package to version 1.11.0

* Tue Dec 06 2022 chendexi <chendexi@kylinos.cn> - 1.11.0-1
- Upgrade package to version 1.11.0

* Fri Aug 05 2022 liukuo <liukuo@kylinos.cn> - 1.10.0-1
- Update to 1.10.0

* Thu Jul 07 2022 OpenStack_SIG <openstack@openeuler.org> - 1.9.3-1
- Upgrade package python3-wsgi-intercept to version 1.9.3

* Sat Feb 20 2021 huangtianhua <huangtianhua223@gmail.com>
- Fix build error

* Thu Jan 28 2021 zhangy <zhangy1317@foxmail.com>
- Add buildrequires

* Thu Dec 31 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
